package com.example.addressbook.service;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import com.example.addressbook.model.AddressBook;
import com.fasterxml.jackson.databind.ObjectMapper;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@WebMvcTest
public class AddressBookControllerTest {
	@Autowired
    private MockMvc mockMvc;
	
	private static ObjectMapper mapper = new ObjectMapper();
	
	
	@Test
	@Order(1)
    public void testCreateAddressBook() throws Exception {
        AddressBook adBook = new AddressBook();
        adBook.setFirstName("testfname");
        adBook.setSurname("testsname");
        adBook.setAddressbookid((long) 1);
        adBook.setPhoneNumber(412343233);
        
        mockMvc.perform( MockMvcRequestBuilders
        	      .post("/api/addressbook")
        	      .content(asJsonString(adBook))
        	      .contentType(MediaType.APPLICATION_JSON)
        	      .accept(MediaType.APPLICATION_JSON))
        	      .andExpect(status().is2xxSuccessful())
        	      .andExpect(MockMvcResultMatchers.jsonPath("$.data").isNotEmpty());    
       }
	
	@Test
	@Order(3)
    public void testUpdateAddressBook() throws Exception {
        AddressBook adBook = new AddressBook();
        adBook.setFirstName("testfname");
        adBook.setSurname("testsname");
        adBook.setAddressbookid((long) 1);
        adBook.setPhoneNumber(412343233);
        
        mockMvc.perform( MockMvcRequestBuilders
        	      .put("/api/updateaddressbook/{addressbookid}",adBook.getAddressbookid())
        	      .content(asJsonString(adBook))
        	      .contentType(MediaType.APPLICATION_JSON)
        	      .accept(MediaType.APPLICATION_JSON))
        	      .andExpect(status().is2xxSuccessful())
        	      .andExpect(MockMvcResultMatchers.jsonPath("$.data").value(1));    
       }
	
	@Test
	@Order(2)
	public void testListOfAddressBooks() throws Exception {
		mockMvc.perform( MockMvcRequestBuilders
			      .get("/api/listaddressbooks")
			      .accept(MediaType.APPLICATION_JSON))
			      .andDo(print())
			      .andExpect(status().isOk())
			      .andExpect(MockMvcResultMatchers.jsonPath("$.data").exists())
			      .andExpect(MockMvcResultMatchers.jsonPath("$.data[*].id").isNotEmpty());
	}
	
	@Test
	@Order(4)
	public void testListOfAddressBook() throws Exception {
		mockMvc.perform( MockMvcRequestBuilders
			      .get("/api/listaddressbook/{addressbookid}", 1)
			      .accept(MediaType.APPLICATION_JSON))
			      .andDo(print())
			      .andExpect(status().isOk())
			      .andExpect(MockMvcResultMatchers.jsonPath("$.data").exists())
			      .andExpect(MockMvcResultMatchers.jsonPath("$.data[*].id").isNotEmpty());
	}
	@Test
	@Order(5)
	public void deleteEmployeeAPI() throws Exception 
	{
		mockMvc.perform( MockMvcRequestBuilders.delete("/api/deleteaddressbook/{id}", 1) )
	        .andExpect(status().isOk());
	}
	
	public static String asJsonString(final Object obj) {
	    try {
	        return new ObjectMapper().writeValueAsString(obj);
	    } catch (Exception e) {
	        throw new RuntimeException(e);
	    }
	}
}

