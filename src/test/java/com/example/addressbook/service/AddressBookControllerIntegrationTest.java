package com.example.addressbook.service;

import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.jdbc.Sql;

import com.example.addressbook.AddressbookApplication;
import com.example.addressbook.controller.AddressBookController;

@SpringBootTest(classes = AddressbookApplication.class, 
webEnvironment = WebEnvironment.RANDOM_PORT)
public class AddressBookControllerIntegrationTest {
	
	@LocalServerPort
	private int port;

	@Autowired
	private TestRestTemplate restTemplate;

	@Sql({ "classpath:schema.sql", "classpath:data.sql" })
	@Test
	public void testListALLAddressBooks() 
	{
		assertTrue(
				this.restTemplate
					.getForObject("http://localhost:" + port + "/listaddressbooks", AddressBookController.class)
					.listAddressBooks().getErrorCode().equals("NoError"));
	}

}
