package com.example.addressbook.dao;

import java.util.List;

import javax.persistence.TypedQuery;

import org.hibernate.Session;

import com.example.addressbook.model.AddressBook;
import com.example.addressbook.util.HibernateUtil;

public class HibernateDAO {
	
	private static HibernateDAO instance;

	private HibernateDAO() {
	}

	public static HibernateDAO getInstance() {
		if (instance == null) {
			instance = new HibernateDAO();
		}
		return instance;
	}

	public Session getSession() {
		return HibernateUtil.getSessionFactory().openSession();
	}
	
	public AddressBook saveAddressBook(Session session, AddressBook addressbook) {
		
		AddressBook adBook = new AddressBook();
		
		adBook.setFirstName(addressbook.getFirstName());
		adBook.setSurname(addressbook.getSurname());
		adBook.setPhoneNumber(addressbook.getPhoneNumber());
		adBook.setAddressbookid(addressbook.getAddressbookid());
		
		session.save(adBook);
		
		return adBook;
	}
	
	public List<AddressBook> listAddressBooks(Session session) {
		
		StringBuffer adBookQuery = new StringBuffer();
		adBookQuery.append("from AddressBook where 1=1 ");
		
		TypedQuery<AddressBook> query = session.createQuery(adBookQuery.toString(), AddressBook.class);

		List<AddressBook> list = query.getResultList();

		return list;

	}
	
	public List<AddressBook> listAddressBook(Session session, AddressBook adBook) {
		
		StringBuffer adBookQuery = new StringBuffer();
		adBookQuery.append("from AddressBook where addressbookid=:addressbookid");
		
		TypedQuery<AddressBook> query = session.createQuery(adBookQuery.toString(), AddressBook.class);

		query.setParameter("addressbookid", adBook.getAddressbookid());
		
		List<AddressBook> list = query.getResultList();

		return list;

	}
	
	public int deleteAddressBook(Session session, Integer id) {
		
		StringBuffer adBookQuery = new StringBuffer();
		adBookQuery.append(" delete from AddressBook where id=:id ");
		
		TypedQuery<?> query = session.createQuery(adBookQuery.toString());

		query.setParameter("id", id);
		
		return query.executeUpdate();

	}
	
	public int updateAddressBook(Session session, AddressBook addressbook) {
		
		StringBuffer adBookQuery = new StringBuffer();
		adBookQuery.append("UPDATE AddressBook SET ");
				
		if(addressbook.getFirstName() != null && !addressbook.getFirstName().equals(""))
			adBookQuery.append(" firstname=:firstname,");
			
		if(addressbook.getSurname() != null && !addressbook.getSurname().equals(""))
			adBookQuery.append(" surname=:surname,");	
		
		if(addressbook.getPhoneNumber() != null)
			adBookQuery.append("  phonenumber=:phonenumber");	
		
		adBookQuery.append(" WHERE addressbookid=:addressbookid");
		
		TypedQuery<?> query = session.createQuery(adBookQuery.toString());

		if(addressbook.getFirstName() != null && !addressbook.getFirstName().equals(""))
			query.setParameter("firstname", addressbook.getFirstName());
		
		if(addressbook.getSurname() != null && !addressbook.getSurname().equals(""))
			query.setParameter("surname", addressbook.getSurname());
		
		if(addressbook.getPhoneNumber() != null)
			query.setParameter("phonenumber", addressbook.getPhoneNumber());
		
		query.setParameter("addressbookid", addressbook.getAddressbookid());
		
		
		return query.executeUpdate();
	}

}
