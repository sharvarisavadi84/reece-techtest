package com.example.addressbook.controller;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.addressbook.dao.HibernateDAO;
import com.example.addressbook.model.AddressBook;
import com.example.addressbook.util.Result;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api")
public class AddressBookController {

	
	@PostMapping("/addressbook")
	public Result createAddressBook(@RequestBody AddressBook addressBook) {
		Result result = new Result();
		try {
			HibernateDAO hibernateDAO = HibernateDAO.getInstance();
			
			Transaction txnSaveadBook = null;
			
			Session session = hibernateDAO.getSession();
			txnSaveadBook= session.beginTransaction();

			AddressBook adBook = hibernateDAO.saveAddressBook(session, addressBook);
			
			txnSaveadBook.commit();
			
			result.setData(adBook);
			result.setSuccess("Entry Added to address book");
			
			return result;
		} catch (Exception e) {
			result.setFailuer(e.getMessage(), "DBError");
			return result;
		}
	}
	
	
	@GetMapping("/listaddressbooks")
	public Result listAddressBooks() {
		Result result = new Result();
		try {
			HibernateDAO hibernateDAO = HibernateDAO.getInstance();
						
			Session session = hibernateDAO.getSession();

			List<AddressBook> adBook = hibernateDAO.listAddressBooks(session);
			
			
			result.setData(adBook);
			result.setSuccess(" List of people in address book");
			
			return result;
		} catch (Exception e) {
			result.setFailuer(e.getMessage(), "DBError");
			return result;
		}
	}

	@GetMapping("/listaddressbook/{addressbookid}")
	public Result listAddressBook(@PathVariable("addressbookid") Integer addressbookid) {
		Result result = new Result();
		try {
			HibernateDAO hibernateDAO = HibernateDAO.getInstance();
						
			Session session = hibernateDAO.getSession();
			
			AddressBook adbook = new AddressBook();
			adbook.setAddressbookid(Long.valueOf(addressbookid));

			List<AddressBook> adBook = hibernateDAO.listAddressBook(session, adbook);
			
			
			result.setData(adBook);
			result.setSuccess(" List of people in address book");
			
			return result;
		} catch (Exception e) {
			result.setFailuer(e.getMessage(), "DBError");
			return result;
		}
		
	}

	
	@DeleteMapping("/deleteaddressbook/{id}")
	public Result deleteAddressBook(@PathVariable("id") Integer id) {
		Result result = new Result();
		try {
			if(id != null) {
			HibernateDAO hibernateDAO = HibernateDAO.getInstance();
						
			Session session = hibernateDAO.getSession();
			Transaction txnDeleteBook = null;
			
			txnDeleteBook= session.beginTransaction();


			int deleted = hibernateDAO.deleteAddressBook(session, id);
			
			txnDeleteBook.commit();
			
			result.setData(deleted);
			result.setSuccess("Entry deleted from address book");
			
			} else {
			
				result.setFailuer("please provide id to delete row", "DBError");
			}

			return result;
		} catch (Exception e) {
			result.setFailuer(e.getMessage(), "DBError");
			return result;
		}
	}
	
	@PutMapping("/updateaddressbook/{addressbookid}")
	public Result updateAddressBook(@PathVariable("addressbookid") Integer addressbookid, @RequestBody AddressBook addressBook) {
		Result result = new Result();
		
		try {
			HibernateDAO hibernateDAO = HibernateDAO.getInstance();
			
			Transaction txnUpdateadBook = null;
			
			Session session = hibernateDAO.getSession();
			txnUpdateadBook= session.beginTransaction();
			
			AddressBook adbook = new AddressBook();
			adbook.setFirstName(addressBook.getFirstName());
			adbook.setSurname(addressBook.getSurname());
			adbook.setPhoneNumber(addressBook.getPhoneNumber());
			adbook.setAddressbookid(Long.valueOf(addressbookid));

			int adBook = hibernateDAO.updateAddressBook(session, adbook);
			
			txnUpdateadBook.commit();
			
			result.setData(adBook);
			result.setSuccess("Row has been updated in database");
			
			return result;
		} catch (Exception e) {
			result.setFailuer(e.getMessage(), "DBError");
			return result;
		}
	
	}

}
