package com.example.addressbook.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "addressbook")
public class AddressBook  {
	
	@Id
	  @GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
	  @GenericGenerator(name = "native", strategy = "native")
	  @Column(name = "ID")
	  private Long id;

	
	@Column(name="addressbookid")
	private Long addressbookid;
	
	@Column(name="firstname")
	private String firstName;
	
	@Column(name="surname")
	private String surname;
	
	@Column(name="phonenumber")
	private Integer phoneNumber;
	
	public AddressBook() {
		
	}

	public AddressBook(String firstName, String surname, Integer phoneNumber) {
		
		this.firstName = firstName;
		this.surname = surname;
		this.phoneNumber = phoneNumber;
	}

	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public Integer getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(Integer phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public Long getAddressbookid() {
		return addressbookid;
	}

	public void setAddressbookid(Long addressbookid) {
		this.addressbookid = addressbookid;
	}
	
	
	
}
