package com.example.addressbook.util;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.domain.EntityScan;

@EntityScan
public class HibernateUtil {
	private static final SessionFactory sessionFactory;
	private static final Logger log = LoggerFactory.getLogger(HibernateUtil.class);


	static {
		try{
			sessionFactory = new Configuration().configure().buildSessionFactory();
			
			
		}catch (Exception e) {
			log.error("HibernateUtil Initial session factory creation failed. "+e);
			throw new ExceptionInInitializerError(e);
		}
	}
	
	public static SessionFactory getSessionFactory(){
		return sessionFactory;
	}
}