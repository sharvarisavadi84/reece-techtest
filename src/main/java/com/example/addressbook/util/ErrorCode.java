package com.example.addressbook.util;

public enum ErrorCode {
	NoError("NO") ,
	GenericError("ERR"),
	DBError("DB");

		public final String error;
		
	ErrorCode(String error) {
		this.error = error;
	}
}
