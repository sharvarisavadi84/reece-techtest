package com.example.addressbook.util;

public class Result {
    
    private String errorCode;
    private boolean error;
    private String message;
    private Object data;

    Result(Object data){
    	ErrorCode er = ErrorCode.valueOf("NoError");
    	this.setErrorCode(er.toString());
    	this.error = false;
    	this.setMessage("");
    	this.setData(data);
    }
    
    public Result() {
		// TODO Auto-generated constructor stub
	}

	public boolean isFailure() {
    	return this.error;
    }
    
    public void setSuccess(String message) {
    	this.error = false;
    	ErrorCode er = ErrorCode.valueOf("NoError");
    	this.setErrorCode(er.toString());
    	this.setMessage(message);
    }
    
    public void setFailuer(String message, String code) {
    	this.error = true;
    	if(code.equals("")) {
    		ErrorCode er = ErrorCode.valueOf("GenericError");
	    	this.setErrorCode(er.toString());
    	} else {
    		ErrorCode er = ErrorCode.valueOf(code);
	    	this.setErrorCode(er.toString());
    	}
    	
    	this.setMessage(message);	    	
    }
    
    public void setResult(Result result){
        this.error = result.error;
        this.errorCode = result.errorCode;
        this.data = result.data;
        this.message = result.message;
    }

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

}