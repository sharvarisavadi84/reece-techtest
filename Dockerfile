#
# Build stage
#
FROM maven:3.8.4-jdk-11 AS build
COPY src /home/app/src
COPY pom.xml /home/app
RUN mvn -f /home/app/pom.xml clean package
#
# Package stage
#
FROM openjdk:11


EXPOSE 8080
COPY --from=build /home/app/target/addressbook-0.0.1-SNAPSHOT.jar /usr/local/lib/addressbook.jar
HEALTHCHECK --interval=15m --timeout=10s --retries=3 --start-period=1m CMD wget --no-verbose --tries=1 --spider http://localhost:8080/api/listaddressbooks || exit 1

ENTRYPOINT ["java","-jar","/usr/local/lib/addressbook.jar"]
