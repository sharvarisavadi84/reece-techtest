1. To Run the docker container use below commands
   # To build the docker container use below
     - docker build -t addressbook:1.0 -f Dockerfile .

   # To run the docker container use below -
     -  docker run --name addressbook --restart=always -p 8080:8080 -d addressbook:1.0
     